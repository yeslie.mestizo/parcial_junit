package com.example.parcial.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class ParcialTest {

	@Test
	public void arreglosIgualesTest() {
		String[] expectativa = {"verde", "negro", "azul"};
		String[] realidad = {"verde", "negro", "azul"};
		assertArrayEquals(expectativa, realidad);
	}
	
	public void areaDeUnTriangulo() {
		int expectativa = 14;
		int realidad = (7*4)/2;
		assertEquals(expectativa, realidad);
	}
	
	public void eliminarFrutasTest() {
		ArrayList<String> frutas = new ArrayList<String>();
		frutas.add("manzana");
		frutas.add("pera");
		frutas.add("mora");
		
		frutas.clear();
		assertTrue(frutas.isEmpty());
	}
	
	public void valoresDistintosTest() {
		int expectativa = 450;
		int realidad = 320+240;
		assertEquals(expectativa, realidad);
	}
	
    public boolean esNumeroPar(int number){
        
        boolean result = false;
        if(number%2 == 0){
            result = true;
        }
        return result;
    }
     
    @Test
    public void numeroParTest(){
    	ParcialTest asft = new ParcialTest();
        assertFalse(asft.esNumeroPar(3));
    }
	
}
